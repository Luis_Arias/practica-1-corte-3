import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import { getDatabase, onValue, ref, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

const firebaseConfig =
{
    apiKey: "AIzaSyCMUtN-i2qg3fr0G0TU5Fv_zG4gVrtDgIk",
    authDomain: "dbweb-f05ea.firebaseapp.com",
    projectId: "dbweb-f05ea",
    storageBucket: "dbweb-f05ea.appspot.com",
    messagingSenderId: "40115385164",
    appId: "1:40115385164:web:3515f921298d47521f55e9"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase();
var btnInsertar = document.getElementById("btnInsertar");
var btnBuscar = document.getElementById("btnBuscar");
var btnActualizar = document.getElementById("btnActualizar");
var btnBorrar = document.getElementById("btnBorrar");
var btnTodos = document.getElementById("btnTodos");
var lista = document.getElementById("lista");
var btnLimpiar = document.getElementById('btnLimpiar');

var matricula = "";
var nombre = "";
var carrera = "";
var genero = "";

function leerInputs() {
    matricula = document.getElementById("matricula").value;
    nombre = document.getElementById("nombre").value;
    carrera = document.getElementById("carrera").value;
    genero = document.getElementById('genero').value;
}
function insertDatos() {
    leerInputs();
    var genero = document.getElementById("genero").value;
    set(ref(db, 'alumnos/' + matricula), {
        nombre: nombre,
        carrera: carrera,
        genero: genero
    }).then((docRef) => {
        alert("registro exitoso");
        mostrarAlumnos();
        console.log("datos" + matricula + nombre + carrera + genero)
    })
        .catch((error) => {
            alert("Error en el registro")
        });
    alert(" se agrego");
};

function mostrarAlumnos() {
    const db = getDatabase();
    const dbRef = ref(db, 'alumnos');
    onValue(dbRef, (snapshot) => {
        lista.innerHTML = ""
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            lista.innerHTML = "<div class='campo'> " + lista.innerHTML + childKey +
                childData.nombre + childData.carrera + childData.genero + "<br> </div>";
            console.log(childKey + ":");
            console.log(childData.nombre)
            // ...
        });
    }, {
        onlyOnce: true
    });
}
function actualizar() {
    leerInputs();
    update(ref(db, 'alumnos/' + matricula), {
        nombre: nombre,
        carrera: carrera, genero: genero
    }).then(() => {
        alert("se realizo actualizacion");
        mostrarAlumnos();
    })
        .catch(() => {
            alert("causo Erro " + error);
        });
}

function escribirInpust() {
    document.getElementById('matricula').value = matricula
    document.getElementById('nombre').value = nombre;
    document.getElementById('carrera').value = carrera;
    document.getElementById('genero').value = genero;
}

function borrar() {
    leerInputs();
    remove(ref(db, 'alumnos/' + matricula)).then(() => {
        alert("se borro");
        mostrarAlumnos();
    })
        .catch(() => {
            alert("causo Erro " + error);
        });
}

function mostrarDatos() {
    leerInputs();
    console.log("mostrar datos ");
    const dbref = ref(db);
    get(child(dbref, 'alumnos/' + matricula)).then((snapshot) => {
        if (snapshot.exists()) {
            nombre = snapshot.val().nombre;
            carrera = snapshot.val().carrera;
            genero = snapshot.val().genero;
            console.log(genero);
            escribirInpust();
        }
        else {
            alert("No existe");
        }
    }).catch((error) => {
        alert("error buscar" + error);
    });
}

function limpiar() {
    lista.innerHTML = "";
    matricula = "";
    nombre = "";
    carrera = "";
    genero = 1;
    escribirInpust();
}
btnInsertar.addEventListener('click', insertDatos);
btnBuscar.addEventListener('click', mostrarDatos);
btnActualizar.addEventListener('click', actualizar);
btnBorrar.addEventListener('click', borrar);
btnTodos.addEventListener('click', mostrarAlumnos);
btnLimpiar.addEventListener('click', limpiar);



